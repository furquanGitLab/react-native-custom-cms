# react-native-custom-cms

custom cms

## Installation

```sh
npm install react-native-custom-cms
```

## Usage

```js
import { multiply } from 'react-native-custom-cms';

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
